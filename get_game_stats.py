import requests
import time
import click
from joblib import Parallel, delayed

STEAMID_URL = 'https://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/'
OWNED_GAMES_URL = 'https://api.steampowered.com/IPlayerService/GetOwnedGames/v1/'
REVIEW_URL = 'https://store.steampowered.com/appreviews/'
APPDETAILS_URL = 'http://store.steampowered.com/api/appdetails/'

STEAM_API_KEY = 'YOUR_STEAM_API_KEY_BELONGS_HERE'

def get_steam_id():
    steam_id = ''
    profile_id = input('Please type in your Steam profile id: ')
    query_params = {
        'key': STEAM_API_KEY,
        'vanityurl': profile_id,
    }

    response = requests.get(STEAMID_URL, params=query_params)
    response_body = response.json().get('response', {})
    if response_body['success'] == 42:
        print(f'No match found with the given profile id {profile_id}. Please try again!')
    elif response_body['success'] == 1:
        steam_id = response_body['steamid']
        print(f'The SteamID for user {profile_id} is {steam_id}.')
    else:
        print('Something went wrong while fetching your SteamID! :(')

    return steam_id

def get_owned_games(steam_id):
    games = []
    query_params = {
        'key': STEAM_API_KEY,
        'steamid': steam_id,
        'include_appinfo': 1,
    }

    response = requests.get(OWNED_GAMES_URL, params=query_params)

    response_body = response.json().get('response', {})
    games = response_body.get('games', [])

    if len(games) > 0:
        print(f'I found {len(games)} games in your Steam library.')
    else:
        print('I did not find any games in your Steam library. Please make sure that you profile is set to public!')

    return games


def get_price_overview(games):
    app_ids = [str(game['appid']) for game in games]

    query_params = {
        'appids': ','.join(app_ids),
        'filters': 'price_overview',
    }

    response = requests.get(APPDETAILS_URL, params=query_params)
    print(response.json(), response.url)

    return response.json()

def fetch_game_metadata(game):
    app_id = game.get('appid', 1)
    query_params = {
        'json': 1,
        'num_per_page': 100,
        'cursor': '*',
        'filter': 'recent',
    }

    response = requests.get(f'{REVIEW_URL}/{app_id}', params=query_params)
    response_body = response.json()

    game_metadata = {
        'appid': app_id,
        'name': game.get('name', ''),
        'personal_playtime': game.get('playtime_forever', 0)/60,
        'average_playtime': 0,
        'playtime_difference': 0,
        'review_score': 0,
        'review_desc': 'No review score',
        'total_reviews': 0,
        'recent_reviews': 0,
    }

    if response_body['success'] == 1:
        total_playtime = 0
        query_summary = response_body['query_summary']
        game_metadata['total_reviews'] = query_summary.get('total_reviews', 0 )

        if game_metadata['total_reviews'] > 0:
            game_metadata['review_score'] = query_summary.get(
                'total_positive', 0) / game_metadata['total_reviews']

        game_metadata['review_desc'] = query_summary.get(
            'review_score_desc', game_metadata['review_desc'])
        reviews = response_body.get('reviews', [])
        game_metadata['recent_reviews'] = len(reviews)

        for review in reviews:
            total_playtime += review['author']['playtime_forever']

        game_metadata['average_playtime'] = total_playtime/len(reviews)/60 if len(reviews) > 0 else 0
        game_metadata['playtime_difference'] = game_metadata['personal_playtime'] - game_metadata['average_playtime']

    return game_metadata


def main():

    steam_id = get_steam_id()

    if steam_id != '':
        owned_games = get_owned_games(steam_id)
        owned_games_metadata = []

        if len(owned_games) > 0:
            with click.progressbar(owned_games, label="Fetching metadata of your games") as bar:
                with Parallel(n_jobs=8) as parallel:
                    owned_games_metadata = parallel(
                        delayed(fetch_game_metadata) (game) for game in bar
                    )

            price_overview = get_price_overview(owned_games_metadata)

            for game in owned_games_metadata:
                game['price_overview'] = price_overview[game['appid']]

            print(owned_games_metadata[0])



main()
